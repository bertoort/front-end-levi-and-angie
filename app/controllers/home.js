import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    dropHearts: function () {

      const imageUrls = [];
      const heartNumber = 20;

      let heartGifUrl = "http://www.picgifs.com/mini-graphics/mini-graphics/hearts/mini-graphics-hearts-655853.gif";

      imageUrls['rain1'] = heartGifUrl;
      imageUrls['rain2'] = heartGifUrl;
      imageUrls['rain3'] = heartGifUrl;
      imageUrls['rain4'] = heartGifUrl;

      let windowWidth = $(window).width();
      let windowHeight = $(window).height();
      let wiggleWidth = 10;

      const inBetweenNumber = (min, max) => Math.round(min + (Math.random() * (max - min)));

      const animateHeart = heart => {
          setTimeout(() => {
              heart.css({
                  left: inBetweenNumber(0, windowWidth) + 'px',
                  top: '-30px',
                  display: 'block',
                  opacity: '0.' + inBetweenNumber(10, 100)
              }).animate({
                  top: (windowHeight - 10) + 'px'
              }, inBetweenNumber(7500, 8000), () => {
                  heart.fadeOut('slow', () => {
                      animateHeart(heart);
                  });
              });
          }, inBetweenNumber(1, 8000));
      };

      // create hearts
      $('<div></div>').attr('id', 'rainDiv')
          .css({
              position: 'fixed',
              width: (windowWidth - 20) + 'px',
              height: '1px',
              left: '0px',
              top: '-5px',
              display: 'block'
          }).appendTo('body');

      // drop hearts
      for (let i = 1; i <= heartNumber; i++) {
          let heart = $('<img/>').attr('src', imageUrls['rain' + inBetweenNumber(1, 4)])
              .css({
                  position: 'absolute',
                  left: inBetweenNumber(0, windowWidth) + 'px',
                  top: '-30px',
                  display: 'block',
                  opacity: '0.' + inBetweenNumber(10, 100),
                  'margin-left': 0
              }).addClass('rainDrop').appendTo('#rainDiv');
          animateHeart(heart);
          heart = null;
      }

      //shake image
      for( var i = 0; i < 4; i++ ) {
       $( "img" ).animate( {
           'margin-left': "+=" + ( wiggleWidth = -wiggleWidth ) + 'px',
           'margin-right': "-=" + wiggleWidth + 'px'
        }, 70);
      }

      //resize values
      $(window).resize(() => {
          windowWidth = $(window).width();
          windowHeight = $(window).height();
      });
    }
  }
});
