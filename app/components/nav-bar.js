import Ember from 'ember';

export default Ember.Component.extend({
  home: false,
  details: false,
  registry: false,
  gallery: false,
  init: function () {
    this._super();
    let current = window.location.pathname.substring(1);
    switch (current) {
      case "details":
        this.details = true;
        break;
      case "registry":
        this.registry = true;
        break;
      case "gallery":
        this.gallery = true;
        break;
      default:
        this.home = true;
    }
  },
  actions: {
    currentTab: function () {
      let current = window.location.pathname.substring(1);
      $('.nav-tab').removeClass('active')
      if (current) {
        $(`.nav-tab.${current}`).addClass('active')
      } else {
        $('.nav-tab.home').addClass('active')
      }
    },
  }
});
