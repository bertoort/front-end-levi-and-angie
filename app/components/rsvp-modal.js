import Ember from 'ember';

export default Ember.Component.extend({
  current: "active",
  isShowingModal: false,
  notify: Ember.inject.service('notify'),
  actions: {
    toggleModal: function() {
      this.toggleProperty('isShowingModal');
    },
    submitRSVP: function () {
      const $checkbox = $('input[name="guest"]:checked')
      const guest = $checkbox.val() === "guest" ? true : false ;
      const data = {
        name: this.name,
        email: this.email,
        song: this.song,
        guest: guest
      };
      if (data.name && data.email) {
        $.post("http://registry.leviandangie.co/registries", {registry: data})
          .done(data => {
            this.set("name", "")
            this.set("email", "")
            this.set("song", "")
            $checkbox.val("")
            this.get('notify').info('Thank you! We\'ll see you soon :)');
            this.send('toggleModal');
          })
          .fail(error => {
            this.get('notify').info('Oh no! Try again later...');
          });
      } else {
        this.get('notify').info('Please include name and email');
      }
    },
  }
});
